//
//  GameScene.swift
//  Flux
//
//  Created by AlbertSamuelMelo on 5/12/16.
//  Copyright (c) 2016 AlbertSamuelMelo. All rights reserved.
//

import SpriteKit
import AVKit
import AVFoundation
class fase1: SKScene, SKPhysicsContactDelegate{
    //variavel da musica de fundo
    var toca: AVAudioPlayer?
  var tela: SKAction!
    var gamePaused = false
    //sistema de som antigo
   // var backgroundAudio = AVPlayer(URL:NSURL(fileURLWithPath:NSBundle.mainBundle().pathForResource("carretafuracao",ofType:"mp3")!))
  
    
    
    var thomas: SKSpriteNode?
    var peca: SKSpriteNode?
    var point: CGPoint?
    var item: SKSpriteNode?
    var professor: SKSpriteNode?
        
    var speedM = (CGFloat(10))
    var allowContact = 0
    
    override func didMoveToView(view: SKView) {
        // seecionar a musica de fundo
        let url = NSURL.fileURLWithPath(
            NSBundle.mainBundle().pathForResource("pegar as coisas",
                ofType: "wav")!)
        
        //musica de fundo
        toca = try!AVAudioPlayer(contentsOfURL: url)
        
        
        toca!.play()
         toca!.numberOfLoops = -1
      
        
        var myLabel = SKLabelNode(fontNamed:"Chalkduster")
        
        myLabel.text = "pausar"
        myLabel.name = "PauseButton"
        myLabel.fontSize = 70
        myLabel.position = CGPoint(x: 1700, y: 1217)
        self.addChild(myLabel)
        ///////////////////

        
        
//        tela = SKAction.playSoundFileNamed("carretafuracao.mp3", waitForCompletion: true)
//        runAction(tela)
        thomas = self.childNodeWithName("thomas") as? SKSpriteNode
        peca = self.childNodeWithName("peca") as? SKSpriteNode
        item = self.childNodeWithName("key") as? SKSpriteNode
        professor = self.childNodeWithName("professor") as? SKSpriteNode

        item?.removeFromParent()
        
        self.physicsWorld.contactDelegate = self
        
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
      
        
        if let touch:UITouch = touches.first {// Get sprite's current position (a.k.a. starting point).
            tela = SKAction.playSoundFileNamed("Passos.mp3", waitForCompletion: false)
            runAction(tela)
            let touchPosition = touch.locationInNode(self)
            //if touchPosition != myLabel.position
            
            let newMoveAction = SKAction.moveByX((CGFloat((Float)(touchPosition.x))-(thomas?.position.x)!), y: 0.0, duration: 0.9)
            
            if thomas?.position.x > touchPosition.x{
                thomas?.xScale = -0.215 //-((thomas?.xScale)!)
            }else{
                thomas?.xScale =  0.215 //1*((thomas?.xScale)!)
            }
            
            thomas!.runAction(newMoveAction)
            point = touchPosition
            
            
        }//jjjj
        /// verificando se botao de pausa foi tocado
        for touch: AnyObject in touches {
            var location = touch.locationInNode(self)
            var node = self.nodeAtPoint(location)
            if (node.name == "PauseButton") || (node.name == "PauseButtonContainer") {
                // se foi tocado chama a func
                showPauseAlert()
            } else {
                
            }
        }
        //////////////////////

        
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        if contact.bodyA.categoryBitMask == contact.bodyB.categoryBitMask {
            if allowContact == 0 || allowContact == 1{
            allowContact = 1
                
            let liftUp = SKAction.scaleTo(2.0, duration: 0.2)
            peca!.runAction(liftUp, withKey: "pickup")
            }else if allowContact == 3 {
            professor?.removeAllActions()
            }
            print(#function)
        }
        
    }
    
    func didEndContact(contact: SKPhysicsContact) {
        let dropDown = SKAction.scaleTo(1.5, duration: 0.2)
        peca!.runAction(dropDown, withKey: "drop")
    }
    
    override func update(currentTime: CFTimeInterval) {
       
        /* Called before each frame is rendered */
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if (allowContact == 1 && peca?.containsPoint(point!) == true){
            takeItem()
        }else if (allowContact == 3 && professor?.containsPoint(point!) == true){
            item?.removeFromParent()
            //para ir para proxima cena
            let scene = cena1 (fileNamed: "cena1")!//vai para primeira cena de corte.
            scene.scaleMode = scaleMode
            let reveal = SKTransition.fadeWithDuration(1)
            self.view?.presentScene(scene, transition: reveal)
            
        }
    }
    
    func takeItem(){
        if allowContact < 3{
        allowContact = 3
        print(#function)
        let dropDown = SKAction.scaleTo(1.5, duration: 0.2)
        peca!.runAction(dropDown, withKey: "drop")
            
            self.addChild(item!)
            peca?.removeFromParent()
            
            let moveMid = SKAction.moveTo(CGPoint(x: 212, y: 1217), duration: 1.0)
            item!.runAction(moveMid)
        }
            let jump = SKAction.moveBy(CGVectorMake(0, 100), duration: 0.5)
            professor?.runAction(SKAction.repeatActionForever(jump))
    }
    
    
    // mostra um alerta que o jogo esta pausado e op para continuar
    func showPauseAlert() {
        self.gamePaused = true
        var alert = UIAlertController(title: "Pause", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Continuar", style: UIAlertActionStyle.Default)  { _ in
            self.gamePaused = false
            //dar o play
            self.toca!.play()

            self.scene!.view?.paused = false
            })
        self.view?.window?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
        scene!.view?.paused = true
        //pausa a musica
        toca!.pause()
        
    }

    
    
    
}
