//
//  fase4.swift
//  Flux
//
//  Created by Saulo de Tarco Neves Oliveira on 17/05/16.
//  Copyright © 2016 AlbertSamuelMelo. All rights reserved.
//
import SpriteKit

class fase4: SKScene, SKPhysicsContactDelegate{
    var tela: SKAction!
    
    var thomas: SKSpriteNode?
    var cartaz: SKSpriteNode?
    var moeda: SKSpriteNode?
    var point: CGPoint?
    var cara: SKSpriteNode?

    var lixo: SKSpriteNode?
    
    var speedM = (CGFloat(10))
    var allowContact = 0
    
    override func didMoveToView(view: SKView) {
        
        
        
        tela = SKAction.playSoundFileNamed("carretafuracao.mp3", waitForCompletion: true)
        runAction(tela)
        thomas = self.childNodeWithName("thomas") as? SKSpriteNode
        moeda = self.childNodeWithName("moeda") as? SKSpriteNode
        cara = self.childNodeWithName("professor") as? SKSpriteNode
     
        
        
        self.physicsWorld.contactDelegate = self
        
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        
        if let touch:UITouch = touches.first {// Get sprite's current position (a.k.a. starting point).
            tela = SKAction.playSoundFileNamed("Passos.mp3", waitForCompletion: false)
            runAction(tela)
            let touchPosition = touch.locationInNode(self)
            
            let newMoveAction = SKAction.moveByX((CGFloat((Float)(touchPosition.x))-(thomas?.position.x)!), y: 0.0, duration: 0.9)
            
            if thomas?.position.x > touchPosition.x{
                thomas?.xScale = -0.215 //-((thomas?.xScale)!)
            }else{
                thomas?.xScale =  0.215 //1*((thomas?.xScale)!)
            }
            
            thomas!.runAction(newMoveAction)
            point = touchPosition
            
            
        }
        
        
        
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        if contact.bodyA.categoryBitMask == contact.bodyB.categoryBitMask {
            if allowContact == 0 || allowContact == 1{
                allowContact = 1
                
                let liftUp = SKAction.scaleTo(2.0, duration: 0.2)
                
            }else if allowContact == 3 {
                cara?.removeAllActions()
               
            }
            print(#function)
        }
    }
    
    func didEndContact(contact: SKPhysicsContact) {
        let dropDown = SKAction.scaleTo(1.5, duration: 0.2)
        
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if (allowContact == 1 && moeda?.containsPoint(point!) == true){
            takeItem()
        }else if (allowContact == 3 && cara?.containsPoint(point!) == true ){
            moeda?.removeFromParent()
            //para ir para proxima cena
            let scene = cena1 (fileNamed: "cena1")!//vai para primeira cena de corte.
            scene.scaleMode = scaleMode
            let reveal = SKTransition.fadeWithDuration(1)
            //self.view?.presentScene(scene, transition: reveal)
            
        }
    }
    
    func takeItem(){
        if allowContact < 3{
            allowContact = 3
            print(#function)
            let dropDown = SKAction.scaleTo(1.5, duration: 0.2)
            moeda!.runAction(dropDown, withKey: "drop")
            
            moeda?.removeFromParent()
            
            let moveMid = SKAction.moveTo(CGPoint(x: 212, y: 1217), duration: 1.0)
            moeda!.runAction(moveMid)
        }
        let jump = SKAction.moveBy(CGVectorMake(0, 100), duration: 0.5)
        cara?.runAction(SKAction.repeatActionForever(jump))
        
    }
}

