//
//  menu.swift
//  Flux
//
//  Created by Saulo de Tarco Neves Oliveira on 16/05/16.
//  Copyright © 2016 AlbertSamuelMelo. All rights reserved.
//

import SpriteKit

class menu: SKScene {
    var play: SKSpriteNode?
    var inicio: SKSpriteNode?
    
    override func didMoveToView(view: SKView) {
        let musica_fundo = SKAudioNode(fileNamed: "carretafuracao.mp3")
        addChild(musica_fundo)
        
        play = SKSpriteNode(imageNamed: "flux")
        play!.position = CGPointMake(frame.width / 2, frame.height / 2)
        self.addChild(play!)
        
        inicio = SKSpriteNode(imageNamed: "Page 1")
        inicio!.position = CGPointMake(600,100)
        self.addChild(inicio!)
        inicio?.name = "inicio"
        
        var myLabel = SKLabelNode(fontNamed:"Chalkduster")
        
        myLabel.text = "iniciar o jogo"
        myLabel.name = "play"
        myLabel.fontSize = 70
        myLabel.position = CGPoint(x: self.frame.size.width/2, y: self.frame.size.height/2)
        self.addChild(myLabel)
        
        
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            let nodeAtTouch = self.nodeAtPoint(touch.locationInNode(self))
            if nodeAtTouch.name == "inicio"{
                
                print("clicou em iniciar o jogo")
                
               
                let scene = fase1(fileNamed: "fase1")!
                 scene.scaleMode = scaleMode
                let transition = SKTransition.moveInWithDirection(.Right, duration: 1)
                self.view?.presentScene(scene, transition: transition)
                
             
              
            }
        }
    }
}
